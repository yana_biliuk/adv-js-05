class Card {
  constructor(post) {
    this.post = post;
    this.element = this.createCard();
    this.deleteButton();
  }

  createCard() {
    const { title, body} = this.post;
    const {name,username,email}=this.post.user
    const card = document.createElement("div");
    card.classList.add("card");
    card.innerHTML = `
        
        <h2>${name} ${username} </h2> <p>${email}</p>
      
            <h2>${title}</h2>
            <p>${body}</p>
        `;
    return card;
  }

  deleteButton() {
    const button = document.createElement("button");
    button.classList.add("button");
    button.textContent = "Delete";
    button.addEventListener("click", () => this.deletePost());
    this.element.append(button);
  }

  deletePost() {
    fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
      method: "DELETE",
    })
      .then((response) => {
        if (response.ok) {
          this.element.remove();
        } else {
          console.error("Error:", response.statusText);
        }
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  }
}
document.addEventListener("DOMContentLoaded", () => {
  Promise.all([
    fetch("https://ajax.test-danit.com/api/json/users").then((response) =>
      response.json()
    ),
    fetch("https://ajax.test-danit.com/api/json/posts").then((response) =>
      response.json()
    ),
  ])
    .then(([users, posts]) => {
      const usersMap = users.reduce((map, user) => {
        map[user.id] = user;

        return map;
      }, {});

      posts.forEach((post) => {
        const user = usersMap[post.userId];
        console.log(user);
        post.user = user;
        const card = new Card(post);

        document.getElementById("container").append(card.element);
      });
    })
    .catch((error) => {
      console.error("Error:", error);
    });
});
